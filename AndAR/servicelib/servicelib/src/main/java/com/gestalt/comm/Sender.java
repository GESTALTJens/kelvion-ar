/**
 * Copyright by GESTALT Robotics 2017
 * Author: tom on 05.12.17.
 */
package com.gestalt.comm;

import java.io.Serializable;

/**
 * Interface for a sender.
 */
public interface Sender<SEND, RESP> {

    /**
     * Is called when data should be send.
     *
     * @param data: Data to send.
     * @return Response from the reciever.
     */
    public RESP send(SEND data);

    /**
     * Is called to close sender.
     */
    public void close();
}
