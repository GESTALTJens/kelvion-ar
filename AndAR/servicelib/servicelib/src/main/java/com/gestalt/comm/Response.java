package com.gestalt.comm;


public enum Response {
    OK,
    SECURITY_ERROR,
    UNKNOWN_HOST,
    SOCKET_ERROR,
    NETWORK_ERROR,
}
