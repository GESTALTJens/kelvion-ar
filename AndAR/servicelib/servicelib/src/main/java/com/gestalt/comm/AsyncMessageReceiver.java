package com.gestalt.comm;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class AsyncMessageReceiver extends Thread {

    private int port = 4444;
    private PrintWriter outWriter;
    private OnMessageReceived messageListener;

    /**
     * Constructor of the class
     * @param messageListener listens for the messages
     */
    public AsyncMessageReceiver(OnMessageReceived messageListener) {
        this.messageListener = messageListener;
    }

    /**
     * Constructor of the class
     * @param messageListener listens for the messages
     * @param port opens the port for incoming messages
     */
    public AsyncMessageReceiver(OnMessageReceived messageListener, int port) {
        this.messageListener = messageListener;
        this.port = port;
    }

    /**
     * Method to send the messages from server to client
     * @param message the message sent by the server
     */
    public void sendMessage(String message){
        if (outWriter != null && !outWriter.checkError()) {
            outWriter.println(message);
            outWriter.flush();
        }
    }

    public int getPort(){
        return port;
    }

    @Override
    public void run() {
        super.run();

        while(true) {
            try {

                ServerSocket serverSocket = new ServerSocket(port);
                Socket client = serverSocket.accept();

                try {

                    outWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(client.getOutputStream())), true);

                    BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                    while (true) {
                        String message = in.readLine();
                        System.out.println("S: Received: " + message);

                        if (messageListener != null) {
                            //call the method messageReceived from ServerBoard class
                            messageListener.messageReceived(message);
                        }
                    }

                } catch (Exception e) {
                    System.out.println("S: Error");
                    e.printStackTrace();
                } finally {
                    client.close();
                    System.out.println("S: Done.");
                }

            } catch (Exception e) {
                System.out.println("S: Error");
                e.printStackTrace();
            }
        }

    }

    /**
     * @brief Declare a callback interface which is called when a message is received
     */
    public interface OnMessageReceived {
        public void messageReceived(String message);
    }

}