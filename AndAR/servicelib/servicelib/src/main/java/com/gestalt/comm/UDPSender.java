/**
 * Copyright by GESTALT Robotics 2017
 * Author: tom on 05.12.17.
 */
package com.gestalt.comm;

import android.util.Log;

import com.gestalt.service.BuildConfig;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Sender to send data via UDP protocol.
 */
public class UDPSender extends AbstractSender<byte[]> {

    private static final String TAG = "UDPSender";
    private DatagramSocket socket;

    public UDPSender(String address, int port) {
        super(address, port);
        initSocket();
    }

    private void initSocket() {
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "UDP Socket creation: SocketException: " + e.getMessage());
            }
        } catch (SecurityException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "UDP Socket creation: SecurityException" + e.getMessage());
            }
        }
    }


    @Override
    public Response send(byte[] data) {
        if (socket == null) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "socket null. reinit.");
            }
            initSocket();
        } else if (socket.isClosed()) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "socket closed. try to reinit.");
            }
            initSocket();
        }

        InetAddress local = null;
        try {
            local = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            return Response.UNKNOWN_HOST;
        }


        int msg_length = data.length;
        DatagramPacket p = new DatagramPacket(data, msg_length, local, port);
        // debugMessage("data.length="+data.length);
        try {
            socket.send(p);
        } catch (SocketException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "SocketException: " + e.getMessage());
            }
            return Response.SOCKET_ERROR;
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                Log.d(TAG, "IOException: " + e.getMessage());
            }
            return Response.NETWORK_ERROR;
        }

        return Response.OK;
    }

    @Override
    public void close() {
        closeSocket();
    }

    private void closeSocket() {
        if (socket != null) {
            socket.close();
        }
    }


    //DEBUG
    private int debugCounter = 0;
    private int debugDropLimit = 20;

    /**
     * Sends only every dropLimit message to logcat.
     *
     * @param message
     */
    private void debugMessage(String message) {
        if (debugCounter < debugDropLimit) {
            debugCounter++;
            return;
        } else {
            Log.d(TAG, message);
            debugCounter = 0;
        }
    }
    //! DEBUG

}
