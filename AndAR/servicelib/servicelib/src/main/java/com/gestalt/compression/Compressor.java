package com.gestalt.compression;

import com.gestalt.comm.Description;

import java.nio.ByteBuffer;

public class Compressor {

    public static ByteBuffer compress(ByteBuffer data, Description description, CompressionType type, int virtualThreadId)
    {
        switch(type)
        {
            case PNG:
                return PNGCompressor.compress(data, description, virtualThreadId);

            case ZIP:
                ZipCompressor compressor = new ZipCompressor();
                return compressor.compress(data, description, virtualThreadId);

            case RAW:
            default:
                return data;
        }
    }
}
