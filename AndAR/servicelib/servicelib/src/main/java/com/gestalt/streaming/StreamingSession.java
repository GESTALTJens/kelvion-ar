package com.gestalt.streaming;

import android.util.Log;
import java.nio.ByteBuffer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.gestalt.comm.Description;
import com.gestalt.comm.SendingTask;
import com.gestalt.comm.TCPSender;
import com.gestalt.service.BuildConfig;


public class StreamingSession extends Thread {
    private String TAG = "StreamingSession";

    private TCPSender sender;

    private VideoFrameMonitoringCallback videoFrameMonitoringCallback = null;
    private PreSendProcessCallback preprocessCallback = null;
    private ExecutorService executor;

    private final int futureCount = 1;
    private int currentFuture = 0;
    private Future[] futures;

    public StreamingSession(String ipAddress, Integer port) {
        sender = new TCPSender(ipAddress, port);
        this.executor = Executors.newFixedThreadPool(futureCount);
        this.futures = new Future[futureCount];
    }

    public void setVideoFrameMonitoringCallback(VideoFrameMonitoringCallback videoFrameMonitoringCallback) {
        this.videoFrameMonitoringCallback = videoFrameMonitoringCallback;
    }

    public void setPreprocessCallback(PreSendProcessCallback preprocessCallback) {
        this.preprocessCallback = preprocessCallback;
    }

    public void streamImage(ByteBuffer data, Description description)
    {
        if(!sender.isOpen())
        {
            sender.open();
        }

        if(sender.readyToSend())
        {
            passToThreadIfAvailable(data, description);
        } else
        {
            logIfDebug(" Skip frame because sender is not ready.");
        }
    }

    private synchronized void passToThreadIfAvailable(ByteBuffer data, Description description)
    {
        callVideoFrameMonitoringCallback(data, description);

        if(nextFutureIsReady()) {
            try {
                submitSendingTask(data, description);
                incrementCurrentFuture();
            } catch (Exception e) {
                logIfDebug("Calling Sending thread " + currentFuture + "throws error: " + e.getMessage());
            }
        }
    }

    private void submitSendingTask(ByteBuffer data, Description description)
    {
        futures[currentFuture] = executor.submit(new SendingTask(data, description, sender, preprocessCallback, currentFuture));
    }

    public void linkSource(Source source)
    {
        source.linkToSession(this);
    }

    @Override
    public void run() {
        super.run();

        while (true) {
        }
    }

    private void incrementCurrentFuture()
    {
        ++currentFuture;
        if(currentFuture >= futureCount)
            currentFuture = 0;
    }

    private boolean nextFutureIsReady()
    {
        return futures[currentFuture] == null || futures[currentFuture].isDone();
    }

    private void logIfDebug(String msg)
    {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, TAG+": "+msg);
        }
    }

    private void callVideoFrameMonitoringCallback(ByteBuffer data, Description description)
    {
        if(videoFrameMonitoringCallback != null)
        {
            videoFrameMonitoringCallback.onVideoFrameRecieved(data, description);
        }
    }

    public interface VideoFrameMonitoringCallback {
        void onVideoFrameRecieved(ByteBuffer data, Description description);
    }

    public interface PreSendProcessCallback {
        ByteBuffer preprocessData(ByteBuffer data, Description description, int virtualThreadId);
    }
}
