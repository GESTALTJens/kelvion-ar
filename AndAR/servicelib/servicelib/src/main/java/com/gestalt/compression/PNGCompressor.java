package com.gestalt.compression;

import android.graphics.Bitmap;

import com.gestalt.comm.Description;


import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;

public class PNGCompressor {

    public static ByteBuffer compress(ByteBuffer data, Description description, int virtualThreadId)
    {
        description.setCompression(CompressionType.PNG.getValue());

        data.rewind();

        byte[] array = ArrayPool.getArray(virtualThreadId, data.capacity() / 3 * 4);

        for(int i = 0; i < array.length; i += 4)
        {
            array[i] = (byte)255;
            array[i+1] = data.get();
            array[i+2] = data.get();
            array[i+3] = data.get();
        }

        Bitmap bmp = Bitmap.createBitmap(description.getWidth(), description.getHeight(), Bitmap.Config.ARGB_8888);
        bmp.copyPixelsFromBuffer(ByteBuffer.wrap(array));

        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 0, outStream);

        description.setDataSize(outStream.size());

        return ByteBuffer.wrap(outStream.toByteArray());
    }
}
