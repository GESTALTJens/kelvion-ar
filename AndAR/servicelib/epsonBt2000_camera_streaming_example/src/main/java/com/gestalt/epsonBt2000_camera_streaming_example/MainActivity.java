package com.gestalt.epsonBt2000_camera_streaming_example;

import android.Manifest;
import android.graphics.ImageFormat;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.gestalt.comm.Description;
import com.gestalt.compression.CompressionType;
import com.gestalt.compression.Compressor;
import com.gestalt.streaming.StreamingSession;
import com.gestalt.comm.ImageType;

import com.gestalt.epsonBt2000_camera_streaming_example.CameraStreamingSource;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import android.util.Log;

/**
 * This example streams an image stream to a given client.
 */
public class MainActivity extends Activity {
    private String TAG = "MainActivity";

    private static final String CLIENT_IP = "192.168.1.7";
    private static final int CLIENT_PORT = 8091;
    private Camera mCamera;
    private CameraStreamingSource imgCallbackAndStreamingSource = new CameraStreamingSource(ImageType.YUV420_888, CompressionType.RAW); //api 15 provides preview images as jpeg.. :(
    private SurfaceTexture mSurfaceTexture = new SurfaceTexture(0);
    //private MediaRecorder mRecorder = new MediaRecorder();


    private static final int[][] RESOLUTIONS = {
            {640, 480},
            {1280, 720},
            {1920, 1080},
    };
    private int mResolutionIndex;
    private int[] getResolution() {
        if (RESOLUTIONS.length <= mResolutionIndex) {
            mResolutionIndex = 0;
        } else if (mResolutionIndex < 0) {
            mResolutionIndex = RESOLUTIONS.length - 1;
        }
        return RESOLUTIONS[mResolutionIndex];
    }
    private void changeResolution() {
        mCamera.stopPreview();
        Camera.Parameters parameters = mCamera.getParameters();
        int[] resolution = getResolution();
        parameters.setPreviewSize(resolution[0], resolution[1]);
        mCamera.setParameters(parameters);
        mCamera.startPreview();
        //setText(resolution);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().setFlags(128, 128);
        //setBodyView(R.layout.activity_main);

        StreamingSession stream = new StreamingSession(CLIENT_IP, CLIENT_PORT);
        stream.linkSource(imgCallbackAndStreamingSource);
        /*stream.setPreprocessCallback(new StreamingSession.PreSendProcessCallback() {
            @Override
            public ByteBuffer preprocessData(ByteBuffer data, Description description, int virtualThreadId) {
                return Compressor.compress(data ,description, CompressionType.RAW, virtualThreadId);
            }
        });
        */

        Thread thread1 = new Thread(imgCallbackAndStreamingSource);
        thread1.start();
        stream.start();
        stopPreviewAndFreeCamera();
        int MY_CAMERA_REQUEST_CODE = 100;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            }
        }


        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_FRONT);
        try {

            Camera.Parameters params=mCamera.getParameters();
            List<Integer> formats=params.getSupportedPreviewFormats();
            for (int i=0; i<formats.size(); i++)
            {
                Log.i(TAG, "Image preview format: "+formats.get(i));
            }

            //resolutions
            List<Camera.Size> sizes=params.getSupportedPreviewSizes();
            for (int i=0; i<sizes.size(); i++)
            {
                Log.i(TAG, "Image preview size: "+sizes.get(i).width+"x"+sizes.get(i).height);
            }

            params.setPreviewSize(640,480);

            mCamera.setParameters( params);
            /*
            Camera.Parameters parameters = mCamera.getParameters();
            //parameters.setEpsonCameraMode((Camera.Parameters.EPSON_CAMERA_MODE_SINGLE_THROUGH_1080P));
            parameters.setPreviewFpsRange(7500, 7500);
            int[] resolution = getResolution();
            parameters.setPreviewSize(resolution[0], resolution[1]);
            mCamera.setParameters(parameters);
            */
            //start cam
            mCamera.reconnect();
            mCamera.setPreviewTexture(mSurfaceTexture);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        mCamera.setPreviewCallback(imgCallbackAndStreamingSource);
        mCamera.startPreview();
    }


    /*protected void printPossibleTrackingResolutions() {
        //read all possible tracking resolutions.

        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String[] cameraIds = cameraManager.getCameraIdList();
            StreamConfigurationMap map = null;

            for (String cameraId : cameraIds) {
                CameraCharacteristics cameraCharacteristics =
                        cameraManager.getCameraCharacteristics(cameraId);
                if(cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) !=
                        CameraCharacteristics.LENS_FACING_FRONT){
                    map = cameraCharacteristics
                            .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                }
            }
            if(map==null) {
                Log.w(TAG, "no appropriate camera found");
                initErrorSpinner();
                return;
            }

            availableResolutions = map.getOutputSizes(mImageFormat);
            initResolutionSpinner();
        }catch (CameraAccessException e) {
            Log.w(TAG, "no access to camera");
            initErrorSpinner();
            return;
        }
    }
    */



    /**
     * When this function returns, mCamera will be null.
     */
    private void stopPreviewAndFreeCamera() {

        if (mCamera != null) {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();

            // Important: Call release() to release the camera for use by other
            // applications. Applications should release the camera immediately
            // during onPause() and re-open() it during onResume()).
            mCamera.release();

            mCamera = null;
        }
    }

   
}