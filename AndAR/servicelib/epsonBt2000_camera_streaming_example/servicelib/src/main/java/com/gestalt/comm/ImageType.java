package com.gestalt.comm;

public enum ImageType {
    DEPTH_16BIT(0), DEPTH_8BIT(1), RGB_8BIT(2), YUV420_888(3);

    private final int value;
    private ImageType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
