package com.gestalt.comm;

import android.util.Log;

import com.gestalt.service.BuildConfig;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.atomic.AtomicBoolean;


public class TCPSender extends AbstractSender<ByteBuffer> {

    private static final String TAG = "TCPSender";
    private Socket socket = new Socket();
    private DataOutputStream dataOutputStream = null;
    private WritableByteChannel outChannel = null;
    private int ConnectionTimeout = 100;
    private ByteBuffer[] buffers;

    public TCPSender(String ipAddress, int port) {
        super(ipAddress, port);
    }

    public Response sendSequential(ByteBuffer... buffers)
    {
        return sendDataIfSenderReady(buffers);
    }

    @Override
    public Response send(ByteBuffer buffer)
    {
        return sendDataIfSenderReady(buffer);
    }

    @Override
    public void close() {
        closeConnection();
    }

    public void open() {
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(ipAddress, port), ConnectionTimeout);
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            outChannel = Channels.newChannel(dataOutputStream);

        } catch (IOException e) {
            logIfDebug("Socket creation: IOException: " + e.getMessage());
        } catch (SecurityException e) {
            logIfDebug("Socket creation: SecurityException" + e.getMessage());
        }

    }

    public boolean readyToSend()
    {
        return isSocketReady() && isOutChannelReady();
    }

    public boolean isOpen() {
        try
        {
            return outChannel.isOpen();
        } catch (Exception e)
        {
            return false;
        }
    }

    private boolean isSocketReady()
    {
        return socket != null && socket.isConnected();
    }

    private boolean isOutChannelReady()
    {
        return outChannel != null && outChannel.isOpen();
    }


    private Response sendDataIfSenderReady(ByteBuffer... buffers)
    {
        if(readyToSend()){
            return sendData(buffers);
        }

        return Response.NETWORK_ERROR;
    }

    private synchronized Response sendData(ByteBuffer... buffers)
    {
        try {

            for (int i = 0; i < buffers.length; i++)
            {
                outChannel.write(buffers[i]);
            }
            dataOutputStream.flush();
        } catch (IOException e){
            logIfDebug("send failed. Close socket.");
            closeConnection();
            return Response.NETWORK_ERROR;
        }

        return Response.OK;
    }

    private void closeConnection() {
        if (socket != null) {
            try {
                this.outChannel.close();
                this.dataOutputStream.close();
                this.socket.close();
            } catch (IOException e) {
                logIfDebug("Socket close: IOException: " + e.getMessage());
            }
        }
    }

    private void logIfDebug(String msg)
    {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, TAG+": "+msg);
        }
    }

}
