package com.gestalt.comm;

import com.gestalt.streaming.StreamingSession;


import java.nio.ByteBuffer;

public class SendingTask implements Runnable {

    private Description description;
    private ByteBuffer data;
    private TCPSender sender;
    private StreamingSession.PreSendProcessCallback preSendProcessCallback;
    private int virtualThreadId;

    public SendingTask(ByteBuffer data, Description description, TCPSender sender, StreamingSession.PreSendProcessCallback preprocessCallback, int virtualThreadId)
    {
        this.description = description;
        this.data = data;
        this.sender = sender;
        this.preSendProcessCallback = preprocessCallback;
        this.virtualThreadId = virtualThreadId;
    }

    @Override
    public void run() {

        if(preSendProcessCallback != null)
            data = preSendProcessCallback.preprocessData(data, description, virtualThreadId);

        sender.sendSequential(ByteBuffer.wrap(description.getBytes()), data);
    }
}
