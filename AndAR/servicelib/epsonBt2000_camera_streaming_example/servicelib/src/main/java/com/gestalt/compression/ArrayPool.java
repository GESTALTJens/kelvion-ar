package com.gestalt.compression;

import java.util.HashMap;

public class ArrayPool {

    private static int allocationCounter = 0;
    private static HashMap<Integer, HashMap<Integer, byte[]>>arrayLayers = new HashMap(10);

    public static int allocations()
    {
        return allocationCounter;
    }

    public static byte[] getArray(int layer, int size)
    {
        createLayerIfNotExist(layer);
        createArrayIfNotExist(layer, size);
        return getArrayFromLayer(layer, size);
    }

    public static void clear()
    {
        allocationCounter = 0;
        arrayLayers = new HashMap(10);
    }

    private static void createLayerIfNotExist(int layer)
    {
        if (arrayLayers.get(layer) == null) {
            arrayLayers.put(layer, new HashMap(2));
        }
    }

    private static void createArrayIfNotExist(int layer, int size)
    {
        if (arrayLayers.get(layer).get(size) == null) {
            arrayLayers.get(layer).put(size, allocateArray(size));
        }
    }

    private static byte[] allocateArray(int size)
    {
        ++allocationCounter;
        return new byte[size];
    }

    private static byte[] getArrayFromLayer(int layer, int size)
    {
        return arrayLayers.get(layer).get(size);
    }
}
