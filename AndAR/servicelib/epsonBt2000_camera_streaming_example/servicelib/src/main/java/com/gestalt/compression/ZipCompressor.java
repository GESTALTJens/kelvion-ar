package com.gestalt.compression;

import java.util.zip.Deflater;
import java.nio.ByteBuffer;

import com.gestalt.comm.Description;

public class ZipCompressor {

    public ByteBuffer compress(ByteBuffer data, Description description, int virtualThreadId)
    {
        byte [] copiedData = ArrayPool.getArray(virtualThreadId, description.getDataSize());
        data.get(copiedData);
        ByteBuffer outBuffer = ByteBuffer.wrap(ArrayPool.getArray(virtualThreadId, description.getDataSize() -1));

        int compressedSize = compressIntoByteBuffer(copiedData, outBuffer, virtualThreadId);

        // TODO: Remove workaround
        byte [] result = new byte[compressedSize];
        outBuffer.get(result, 0, compressedSize);

        adjustDescription(description, compressedSize);

        return ByteBuffer.wrap(result);
    }

    private int compressIntoByteBuffer(byte[] data, ByteBuffer destination, int virtualThreadId)
    {

        Deflater deflater = new Deflater();

        deflater.setInput(data);
        deflater.finish();

        int compressedSize = 0;
        byte[] buffer = FixSizeArrayPool.getArray(virtualThreadId);

        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            destination.put(buffer, 0, count);
            compressedSize += count;
        }

        destination.rewind();

        return compressedSize;
    }

    private static void adjustDescription(Description description, int size)
    {
        description.setDataSize(size);
        description.setCompression(CompressionType.ZIP.getValue());
    }
}
