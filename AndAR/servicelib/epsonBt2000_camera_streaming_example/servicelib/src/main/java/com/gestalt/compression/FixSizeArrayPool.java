package com.gestalt.compression;

import java.util.HashMap;

public class FixSizeArrayPool {

    public static final int bufferSize = 2048;
    public static int allocationCounter = 0;
    public static HashMap<Integer ,byte[]> arrayLayers = new HashMap(10);

    public static int getBufferSize() {
        return bufferSize;
    }

    public static int allocations()
    {
        return allocationCounter;
    }

    public static byte[] getArray(int layer)
    {
        createIfNotExist(layer);
        return arrayLayers.get(layer);
    }

    public static void clear()
    {
        allocationCounter = 0;
        arrayLayers = new HashMap(10);
    }

    private static void createIfNotExist(int layer)
    {
        if (arrayLayers.get(layer) == null) {
            arrayLayers.put(layer, allocateArray(bufferSize));
        }
    }

    private static byte[] allocateArray(int size)
    {
        ++allocationCounter;
        return new byte[size];
    }

}
