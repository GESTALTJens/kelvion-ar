package com.gestalt.comm;

import com.gestalt.comm.ImageType;

import java.nio.ByteBuffer;

public class Description {

    private final static int BITS_PER_BYTE = 8;
    private final static int BUFFER_SIZE = (Long.SIZE + Integer.SIZE * 4 + Byte.SIZE) / BITS_PER_BYTE;

    private long timestamp = 0;
    private int width = 0;
    private int height = 0;
    private ImageType imageType = ImageType.DEPTH_16BIT;
    private byte compression = 0;
    private int dataSize = 0;

    private byte[] bytes = null;

    /**
     * set to true if any set-method is called. make sure the backed array will have new generation
     */
    private boolean descriptionChanged = false;

    public Description(){}

    public Description(byte[] description)
    {
        fromBytes(description);
    }

    public byte[] getBytes() {
        if(bytes == null || descriptionChanged) {
            createBytes();
        }

        return bytes;
    }

    private void createBytes() {
        ByteBuffer data = ByteBuffer.allocateDirect(BUFFER_SIZE);

        data.put(compression);
        data.putInt(imageType.getValue());
        data.putInt(width);
        data.putInt(height);
        data.putLong(timestamp);
        data.putInt(dataSize);

        bytes = new byte[data.capacity()];
        data.rewind();
        data.get(bytes, 0, bytes.length);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        // descriptionChanged = true; // no need cause direct change in backed array
        if(bytes != null) {
            ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
            byteBuffer.putLong(13, timestamp);
        }
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
        descriptionChanged = true;
    }

    public ImageType getImageType() {
        return imageType;
    }


    public int getDataSize() {
        return dataSize;
    }

    public void setDataSize(int data_size) {
        this.dataSize = data_size;
        descriptionChanged = true;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
        descriptionChanged = true;
    }

    public byte getCompression() {
        return compression;
    }

    public void setCompression(byte compression) {
        this.compression = compression;
        descriptionChanged = true;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
        descriptionChanged = true;
    }

    public void fromBytes(byte[] description) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(description);

        compression = byteBuffer.get(0);
        imageType = ImageType.values()[byteBuffer.getInt(1)];
        width = byteBuffer.getInt(5);
        height = byteBuffer.getInt(9);
        timestamp = byteBuffer.getLong(13);
        dataSize = byteBuffer.getInt(21);
        descriptionChanged = true;
    }

    public int getRawImageSize()
    {
        int pixels = height * width;
        int bytes_per_pixel = getBytesPerPixel();
        return pixels * bytes_per_pixel;

    }

    public int getBytesPerPixel()
    {
        switch (imageType)
        {
            case DEPTH_8BIT:
                return 1;
            case DEPTH_16BIT:
                return 2;
            case RGB_8BIT:
                return 3;
        }
        return 0;
    }
}
