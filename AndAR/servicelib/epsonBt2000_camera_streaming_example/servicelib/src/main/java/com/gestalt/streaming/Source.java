package com.gestalt.streaming;

public interface Source {

    void linkToSession(StreamingSession session);
}
