package com.gestalt.compression;


public enum CompressionType {
    RAW((byte)0), PNG((byte)1), ZIP((byte)2), BLOSC((byte)3), JPEG((byte)4);

    private final byte value;
    private CompressionType(byte value) {
        this.value = value;
    }

    public byte getValue() {
        return value;
    }
}