/**
 * Copyright by GESTALT Robotics 2017
 * Author: tom on 05.12.17.
 */
package com.gestalt.comm;

/**
 * An interface class for a sender via ip protocol.
 */
public abstract class AbstractSender<SEND> implements Sender<SEND, Response> {
    protected String ipAddress;
    protected int port;

    /**
     * @param ipAddress: IP address of the receiver.
     * @param port:      Port to send data to.
     */
    public AbstractSender(String ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    /**
     * @return IP-Address of the receiver.
     */
    public String getIpAddress() {
        return ipAddress;
    }

    /**
     * @param ipAddress: IP-Address of the receiver.
     */
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    /**
     * @return Port to send data to.
     */
    public int getPort() {
        return port;
    }

    /**
     * @param port: Port to send data to.
     */
    public void setPort(int port) {
        this.port = port;
    }
}
