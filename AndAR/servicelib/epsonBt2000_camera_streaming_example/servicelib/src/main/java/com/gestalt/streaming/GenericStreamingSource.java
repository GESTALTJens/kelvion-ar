package com.gestalt.streaming;

import com.gestalt.comm.Description;
import com.gestalt.compression.CompressionType;
import com.gestalt.comm.ImageType;

import java.nio.ByteBuffer;

public class GenericStreamingSource implements Source, Runnable {

    private StreamingSession streamingSession = null;
    private Description description;
    private int millisecondsBetweenSend;

    private byte[] data;

    private int lastPosModified = -1;

    public GenericStreamingSource(int height, int width, ImageType imageType, int millisecondsBetweenSend)
    {
        this.description = new Description();
        description.setHeight(height);
        description.setWidth(width);
        description.setImageType(imageType);
        description.setCompression(CompressionType.RAW.getValue());
        description.setDataSize(height * width * description.getBytesPerPixel());

        this.millisecondsBetweenSend = millisecondsBetweenSend;

        this.data = new byte[description.getDataSize()];
    }

    @Override
    public void linkToSession(StreamingSession session)
    {
        this.streamingSession = session;
    }

    @Override
    public void run() {
        while(true)
        {
            if(streamingSession != null) {
                this.streamingSession.streamImage(ByteBuffer.wrap(data), description);
                modifyData();
            }

            try
            {
                Thread.sleep(millisecondsBetweenSend);
            } catch(Exception e)
            {
            }
        }
    }

    private void modifyData()
    {
        for(int i = 0; i < description.getWidth() * description.getBytesPerPixel(); ++i)
        {
            ++lastPosModified;

            if(lastPosModified >= description.getDataSize())
            {
              lastPosModified = 0;
            }

            if(data[lastPosModified] < (byte)127) {
                data[lastPosModified] = (byte) 127;
            } else
            {
                data[lastPosModified] = (byte) -1; // = 255 in 2-complement
            }
        }
    }
}
