package edu.dhbw.andar.util;

import com.gestalt.comm.Description;
import com.gestalt.compression.CompressionType;
import com.gestalt.comm.ImageType;
import com.gestalt.streaming.Source;
import com.gestalt.streaming.StreamingSession;


import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.graphics.YuvImage;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ListView;
import android.util.TimingLogger;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class CameraStreamingSource implements Source, Runnable {
    private String TAG = "CameraStreamingSource";
    public StreamingSession streamingSession = null;
    public ImageType imageType;
    public CompressionType compression_type;
    public Description description = new Description();
    public byte[] last_image_data;
    long millisecondsBetweenSend=2;
    public boolean new_data_to_send=false;
    public Lock mutex = new ReentrantLock();


    public CameraStreamingSource(ImageType imageType, CompressionType compression_type) {
        this.imageType = imageType;
        this.compression_type=compression_type;
    }


    @Override
    public void run() {
        while(true)
        {
            if(streamingSession != null) {
                if (new_data_to_send) {
                    try {
                        if(mutex.tryLock(500, TimeUnit.MILLISECONDS)){
                            this.streamingSession.streamImage(ByteBuffer.wrap(last_image_data), description);
                            new_data_to_send=false;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }finally{
                        //release lock
                        mutex.unlock();
                    }

                }
            }

            try
            {
                Thread.sleep(millisecondsBetweenSend);
            } catch(Exception e)
            {
            }
        }
    }

/*
    @Override
    public void onPreviewFrame(byte[] image_data, Camera mCamera) {
        Log.i(TAG, "onPreviewFrame called");

        Size previewSize = mCamera.getParameters().getPreviewSize();
        int width = previewSize.width;
        int height = previewSize.height;
        Log.i(TAG, "Building bitmap from the preview: "+width+"x"+height);

        try {
            if(mutex.tryLock(500, TimeUnit.MILLISECONDS)){
                description.setImageType(imageType);
                description.setCompression(compression_type.getValue());
                description.setWidth(previewSize.width);
                description.setHeight(previewSize.height);
                description.setTimestamp(System.currentTimeMillis());
                description.setDataSize(image_data.length);
                Log.i(TAG, "Image data size: "+image_data.length);
                last_image_data=image_data;
                new_data_to_send=true;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally{
            //release lock
            mutex.unlock();
        }



    }
*/



    @Override
    public void linkToSession(StreamingSession session)
    {
        this.streamingSession = session;
    }

}
