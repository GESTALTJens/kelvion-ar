/**
	Copyright (C) 2009,2010  Tobias Domhan

    This file is part of AndOpenGLCam.

    AndObjViewer is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AndObjViewer is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AndObjViewer.  If not, see <http://www.gnu.org/licenses/>.
 
 */
package edu.dhbw.andar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.Thread.UncaughtExceptionHandler;


import edu.dhbw.andar.exceptions.AndARRuntimeException;
import edu.dhbw.andar.interfaces.MarkerVisibilityListener;
import edu.dhbw.andar.interfaces.OpenGLRenderer;
import edu.dhbw.andar.util.IO;
import edu.dhbw.andar.util.Utils;
import edu.dhbw.andopenglcam.R;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Debug;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.SurfaceHolder.Callback;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.btutil.KeyAssign;
import java.util.Observable;
import java.util.Observer;
import java.lang.Object;
import android.util.Log;

import android.view.Window;
import android.view.WindowManager;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import jp.epson.moverio.H725.DisplayControl;

public abstract class AndARActivity extends  Activity implements Callback,UncaughtExceptionHandler{
	private GLSurfaceView glSurfaceView;
	private ImageView imgView;
	private Camera camera;
	public AndARRenderer renderer;
	private Resources res;
	private CameraPreviewHandler cameraHandler;
	private boolean mPausing = false;
	private ARToolkit artoolkit;
	private CameraStatus camStatus = new CameraStatus();
	private boolean surfaceCreated = false;
	private SurfaceHolder mSurfaceHolder = null;
	private Preview previewSurface;
	private boolean startPreviewRightAway;

	private final int TAPPED = 1;

	private DisplayControl mDisplayControl;
	private SensorManager mSensorManager;



	public AndARActivity() {
		startPreviewRightAway = true;
	}
	
	public AndARActivity(boolean startPreviewRightAway) {
		this.startPreviewRightAway = startPreviewRightAway;
	}

	private final int[] IMAGES = {
			R.drawable.bloc,
			R.drawable.bloc2,
			R.drawable.bloc3,
			R.drawable.bloc4,
			R.drawable.bloc5,
			R.drawable.bloc6,
			R.drawable.bloc7,
			R.drawable.bloc8,

	};

	private final int[] IMAGES2 = {
			R.drawable.dd01,
			R.drawable.dd02,
			R.drawable.dd03,
			R.drawable.dd04,
			R.drawable.dd05,
			R.drawable.dd06,
			R.drawable.dd07,
			R.drawable.dd08,
			R.drawable.dd09,
			R.drawable.dd10,
			R.drawable.dd11,
			R.drawable.dd12,
			R.drawable.dd13,
			R.drawable.dd14,
			R.drawable.dd15,
			R.drawable.dd16,
			R.drawable.dd17,
			R.drawable.dd18,
			R.drawable.dd19,


	};
	public int mImageIndex=0;
	public int mImageIndex2=0;
	public int marker_vis=0;


	public void writeToFile(String data) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput("config.txt", Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		}
		catch (IOException e) {
			Log.e("Exception", "File write failed: " + e.toString());
		}
	}

	public String readFromFile() {

		String ret = "";

		try {
			InputStream inputStream = this.openFileInput("config.txt");

			if ( inputStream != null ) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ( (receiveString = bufferedReader.readLine()) != null ) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		}
		catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());
			ret = "192.168.1.7";
		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());
			ret = "192.168.1.7";
		}

		return ret;
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (event.getKeyCode()) {
				case KeyEvent.KEYCODE_DPAD_CENTER:
					//switchMode();
					break;
				case KeyEvent.KEYCODE_F1:
					//setImage(0);
					break;
				case KeyEvent.KEYCODE_F2:
					finish();
					break;
				case KeyEvent.KEYCODE_F3:
					//setImage(1);
					break;
				case KeyEvent.KEYCODE_F4:
					//setImage(2);
					break;

				case KeyEvent.KEYCODE_DPAD_LEFT:
					if(marker_vis==1)
						mImageIndex--;
					else if (marker_vis==2)
						mImageIndex2--;
					changeImage(marker_vis);
					break;
				case KeyEvent.KEYCODE_DPAD_RIGHT:
					if(marker_vis==1)
						mImageIndex++;
					else if (marker_vis==2)
						mImageIndex2++;
					changeImage(marker_vis);
					break;
				default:
					break;
			}
		}


		return super.dispatchKeyEvent(event);
	}
	private void changeImage(int marker) {
		if (KeyAssign.getKeyAssignMode() == KeyAssign.KEYASSIGN_MODE_DEFAULT) {
			if(marker == 2)
			{
				if (IMAGES2.length <= mImageIndex2) {
					mImageIndex2 = 0;
				} else if (mImageIndex2 < 0) {
					mImageIndex2 = IMAGES2.length - 1;
				}
				setImage(mImageIndex2, marker);
			}
			else if(marker ==1)
			{
				if (IMAGES.length <= mImageIndex) {
					mImageIndex = 0;
				} else if (mImageIndex < 0) {
					mImageIndex = IMAGES.length - 1;
				}
				setImage(mImageIndex, marker);
			}


		}
	}

	private void setImage(int index, int marker) {
		if (marker ==2) {
			if (0 <= index && index < IMAGES2.length) {
				imgView.setImageResource(IMAGES2[index]);
				mImageIndex2 = index;
			}
		}
		else if (marker == 1)
		{
			if (0 <= index && index < IMAGES.length) {
				imgView.setImageResource(IMAGES[index]);
				mImageIndex = index;
			}

		}
	}
	//private java.lang.String filename = "/storage/emulated/0/logo2.png";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Thread.currentThread().setUncaughtExceptionHandler(this);
        res = getResources();
        
        artoolkit = new ARToolkit(res, getFilesDir());

        setFullscreen();
        disableScreenTurnOff();
        //orientation is set via the manifest
        
        try {
			IO.transferFilesToPrivateFS(getFilesDir(),res);
		} catch (IOException e) {
			e.printStackTrace();
			throw new AndARRuntimeException(e.getMessage());
		}
		FrameLayout frame = new FrameLayout(this);
		previewSurface = new Preview(this);
				
        glSurfaceView = new GLSurfaceView(this);
		renderer = new AndARRenderer(res, artoolkit, this);
		renderer.inv_mode=false;
		cameraHandler = new CameraPreviewHandler(this, glSurfaceView, renderer, res, artoolkit, camStatus);
        glSurfaceView.setRenderer(renderer);
        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        glSurfaceView.getHolder().addCallback(this);

		imgView = new ImageView(this);//(ImageView) findViewById(R.id.image);//
		imgView.setVisibility(View.GONE);
		imgView.setImageResource(R.drawable.bloc);

		//previewSurface.setVisibility(View.VISIBLE);

        frame.addView(glSurfaceView);
        frame.addView(previewSurface);

        frame.addView(imgView);

        setContentView(frame);
        if(Config.DEBUG)
        	Debug.startMethodTracing("AndAR");

		MarkerVisibilityListener mylistener = new MarkerVisibilityListener() {
			@Override
			public void makerVisibilityChanged(final boolean visible) {
				Utils.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						// UI updation related code.

						changeImageVisState(visible);


					}
				});
			}
		};
		artoolkit.addVisibilityListener(mylistener);
		// Make Fullscreen
		Window win = getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		winParams.flags |= 0x80000000;
		win.setAttributes(winParams);

    }




    public void changeImageVisState(boolean vis)
	{
		//Log.i("JEN", "Change vis: "+vis);
		int v = View.INVISIBLE;
		if (vis ==false)
		{
			v= View.INVISIBLE;
			marker_vis= 0;
			imgView.setVisibility(v);
			artoolkit.lastNumMarkers=0;
			return;
		}
		else
		{
			//Log.i("JEN", "Currnumakers: "+artoolkit.arobjects.size());
			//gehe beide marker durch
			for(int i = 0; i < artoolkit.arobjects.size(); i++) {

				//Log.i("JEN", "Currnumakers: "+artoolkit.arobjects.size());
				double x = artoolkit.arobjects.elementAt(i).getTransMatrix()[3];
				double y = artoolkit.arobjects.elementAt(i).getTransMatrix()[7];
				double z = artoolkit.arobjects.elementAt(i).getTransMatrix()[11];
				double abstand = Math.sqrt(x*x+y*y+z*z);
				if (artoolkit.arobjects.get(i).isVisible()&&abstand > 250 && abstand < 1500){



				marker_vis = artoolkit.arobjects.get(i).markernumber;
				Log.i("JEN", "Abstand: " + abstand);
				v = View.VISIBLE;
				if (marker_vis == 1) {
					setImage(mImageIndex, 1);
				} else if (marker_vis == 2) {
					setImage(mImageIndex2, 2);
				}
				artoolkit.lastNumMarkers = 1;
				imgView.setVisibility(v);
				return;
			}
		}
	}
	marker_vis= 0;
	imgView.setVisibility(v);
	artoolkit.lastNumMarkers=0;

}

    
    /**
     * Set a renderer that draws non AR stuff. Optional, may be set to null or omited.
     * and setups lighting stuff.
     * @param customRenderer
     */
    public void setNonARRenderer(OpenGLRenderer customRenderer) {
		renderer.setNonARRenderer(customRenderer);
	}

    /**
     * Avoid that the screen get's turned off by the system.
     */
	public void disableScreenTurnOff() {
    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
    			WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
	/**
	 * Set's the orientation to landscape, as this is needed by AndAR.
	 */
    public void setOrientation()  {
    	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }
    
    /**
     * Maximize the application.
     */
    public void setFullscreen() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
   
    public void setNoTitle() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    } 
    
    @Override
    protected void onPause() {
    	mPausing = true;
        this.glSurfaceView.onPause();
        super.onPause();
        finish();
        if(cameraHandler != null)
        	cameraHandler.stopThreads();
		/*if (mSensorManager != null) {
			mSensorManager.unregisterListener(this);
		}*/
    }


    @Override
    protected void onDestroy() {
    	super.onDestroy();    	
    	System.runFinalization();
    	if(Config.DEBUG)
    		Debug.stopMethodTracing();
    }
    
    

    @Override
    protected void onResume() {
    	mPausing = false;
    	glSurfaceView.onResume();
        super.onResume();
		setImage(0,1);


    }
    
    /* (non-Javadoc)
     * @see android.app.Activity#onStop()
     */
    @Override
    protected void onStop() {
    	super.onStop();
    }
    
    /**
     * Open the camera.
     */
    private void openCamera()  {
    	if (camera == null) {
	    	//camera = Camera.open();
    		camera = CameraHolder.instance().open();
    		   		    		
    		
    		
    		try {
				camera.setPreviewDisplay(mSurfaceHolder);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			CameraParameters.setCameraParameters(camera, 
					previewSurface.getWidth(), previewSurface.getHeight());
	        
	        if(!Config.USE_ONE_SHOT_PREVIEW) {
	        	camera.setPreviewCallback(cameraHandler);	 
	        } 
			try {
				cameraHandler.init(camera);
			} catch (Exception e) {
				e.printStackTrace();
			}			
    	}
    }
    
    private void closeCamera() {
        if (camera != null) {
        	CameraHolder.instance().keep();
        	CameraHolder.instance().release();
        	camera = null;
        	camStatus.previewing = false;
        }
    }
    
    /**
     * Open the camera and start detecting markers.
     * note: You must assure that the preview surface already exists!
     */
    public void startPreview() {
    	if(!surfaceCreated) return;
    	if(mPausing || isFinishing()) return;
    	if (camStatus.previewing) stopPreview();
    	openCamera();
		camera.startPreview();
		camStatus.previewing = true;
    }
    
    /**
     * Close the camera and stop detecting markers.
     */
    private void stopPreview() {
    	if (camera != null && camStatus.previewing ) {
    		camStatus.previewing = false;
            camera.stopPreview();
         }
    	
    }

	/* The GLSurfaceView changed
	 * @see android.view.SurfaceHolder.Callback#surfaceChanged(android.view.SurfaceHolder, int, int, int)
	 */
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	/* The GLSurfaceView was created
	 * The camera will be opened and the preview started 
	 * @see android.view.SurfaceHolder.Callback#surfaceCreated(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		surfaceCreated = true;			
	}


	/* GLSurfaceView was destroyed
	 * The camera will be closed and the preview stopped.
	 * @see android.view.SurfaceHolder.Callback#surfaceDestroyed(android.view.SurfaceHolder)
	 */
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

	}
	
	/**
	 * @return  a the instance of the ARToolkit.
	 */
	public ARToolkit getArtoolkit() {
		return artoolkit;
	}	
	
	/**
	 * Take a screenshot. Must not be called from the GUI thread, e.g. from methods like
	 * onCreateOptionsMenu and onOptionsItemSelected. You have to use a asynctask for this purpose.
	 * @return the screenshot
	 */
	public Bitmap takeScreenshot() {
		Log.i("JEN", "tAKE sHOT");
		return renderer.takeScreenshot();
	}	
	
	/**
	 * 
	 * @return the OpenGL surface.
	 */
	public SurfaceView getSurfaceView() {
		return glSurfaceView;
	}
	
	class Preview extends SurfaceView implements SurfaceHolder.Callback {
	    SurfaceHolder mHolder;
	    Camera mCamera;
	    private int w;
	    private int h;
	    
	    Preview(Context context) {
	        super(context);
	        
	        // Install a SurfaceHolder.Callback so we get notified when the
	        // underlying surface is created and destroyed.
	        mHolder = getHolder();
	        mHolder.addCallback(this);
	        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	    }

	    public void surfaceCreated(SurfaceHolder holder) {
	    }

	    public void surfaceDestroyed(SurfaceHolder holder) {
	        // Surface will be destroyed when we return, so stop the preview.
	        // Because the CameraDevice object is not a shared resource, it's very
	        // important to release it when the activity is paused.
	        stopPreview();
	        closeCamera();
	        mSurfaceHolder = null;
	    }

	    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
	    	this.w=w;
	    	this.h=h;
	    	mSurfaceHolder = holder;
	    	if(startPreviewRightAway)
	    		startPreview();
	    }
	    
	    public int getScreenWidth() {
	    	return w;
	    }
	    
	    public int getScreenHeight() {
	    	return h;
	    }

	}
}