package edu.dhbw.andar.pub;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.hardware.Sensor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import edu.dhbw.andar.ARToolkit;
import edu.dhbw.andar.AndARActivity;
import edu.dhbw.andar.AndARRenderer;
import edu.dhbw.andar.exceptions.AndARException;
import edu.dhbw.andopenglcam.R;

/**
 * Example of an application that makes use of the AndAR toolkit.
 * @author Tobi
 *
 */
public class CustomActivity extends AndARActivity {
	
	private final int MENU_SCREENSHOT = 0;
    private final int MENU_IP = 1;
    private final int MENU_CAM = 2;
	private AndARRenderer _rend;
	CustomObject someObject;
	ARToolkit artoolkit;
	AndARRenderer _rendi;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		CustomRenderer renderer = new CustomRenderer();//optional, may be set to null
		super.setNonARRenderer(renderer);//or might be omited
		try {
			artoolkit = super.getArtoolkit();
			/*someObject = new CustomObject
				("logo", "patt.kanji", 40.0, new double[]{0,0},1);
			artoolkit.registerARObject(someObject);
			someObject = new CustomObject
					("kev", "patt.hiro", 40.0, new double[]{0,0},2);
			artoolkit.registerARObject(someObject);
			*/
			/*someObject = new CustomObject
					("logo", "patt.log", 40.0, new double[]{0,0},1);
			artoolkit.registerARObject(someObject);
			someObject = new CustomObject
					("kev", "patt.kev", 40.0, new double[]{0,0},2);
			artoolkit.registerARObject(someObject);
			*/someObject = new CustomObject
					("logo", "patt.lo", 40.0, new double[]{0,0},1);
			artoolkit.registerARObject(someObject);
			/*someObject = new CustomObject
					("logo", "patt.ko", 40.0, new double[]{0,0},1);
			artoolkit.registerARObject(someObject);*/
			someObject = new CustomObject
					("kev", "patt.k", 40.0, new double[]{0,0},2);
			artoolkit.registerARObject(someObject);

			//someObject = new CustomObject
			//("test", "android.patt", 80.0, new double[]{0,0});
			//artoolkit.registerARObject(someObject);
			//someObject = new CustomObject
			//("test", "barcode.patt", 80.0, new double[]{0,0});
			//artoolkit.registerARObject(someObject);
			
		} catch (AndARException ex){
			//handle the exception, that means: show the user what happened
			System.out.println("");
		}			
	}

	/**
	 * Inform the user about exceptions that occurred in background threads.
	 * This exception is rather severe and can not be recovered from.
	 * Inform the user and shut down the application.
	 */
	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		Log.e("AndAR EXCEPTION", ex.getMessage());
		finish();
	}	
	
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {		
		/*menu.add(0, MENU_SCREENSHOT, 0, getResources().getText(R.string.takescreenshot))
                .setIcon(R.drawable.screenshoticon);*/

        menu.add(0,MENU_IP, 0, "Set IP")
                .setIcon(R.drawable.icon);

        menu.add(0, MENU_CAM, 0, "Camera Img")
                .setIcon(R.drawable.icon);

		return true;
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*if(item.getItemId()==1) {
			artoolkit.unregisterARObject(someObject);
		} else if(item.getItemId()==0) {
			try {
				someObject = new CustomObject
				("test", "patt.hiro", 80.0, new double[]{0,0});
				artoolkit.registerARObject(someObject);
			} catch (AndARException e) {
				e.printStackTrace();
			}
		}*/
		switch(item.getItemId()) {
		case MENU_SCREENSHOT:
        new TakeAsyncScreenshot().execute();
        break;
        case MENU_IP:
            //
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(CustomActivity.this);
            // Setting Dialog Title
            alertDialog.setTitle("Client IP");
            alertDialog.setMessage("Enter or change client IP");

            final EditText input = new EditText(this);
            String def = this.readFromFile();
            input.setText(def);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            input.setLayoutParams(lp);
            alertDialog.setView(input);
            alertDialog.setIcon(R.drawable.icon);


            alertDialog.setPositiveButton("YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String _ip = input.getText().toString();
							Log.e("JEN", _ip);

                                    writeToFile(_ip);


                        }
                    }
            );
            alertDialog.setNegativeButton("NO",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    }
            );
			alertDialog.show();
			Log.e("JEN", "alert dia end");
            break;

            case MENU_CAM:
			if (renderer.inv_mode==false)
				renderer.inv_mode=true;
			else
				renderer.inv_mode=false;



            break;
        }

		return true;
	}



	class TakeAsyncScreenshot extends AsyncTask<Void, Void, Void> {
		
		private String errorMsg = null;

		@Override
		protected Void doInBackground(Void... params) {
			Bitmap bm = takeScreenshot();
			Log.i("JEN", "Shot async");
			FileOutputStream fos;
			try {
				fos = new FileOutputStream(Environment.DIRECTORY_DOWNLOADS + File.separator +new Date().getTime()+".png");
				bm.compress(CompressFormat.PNG, 100, fos);
				fos.flush();
				fos.close();
				//Log.i("JEN", "Shot saved");
			} catch (FileNotFoundException e) {
				errorMsg = e.getMessage();
				e.printStackTrace();
			} catch (IOException e) {
				errorMsg = e.getMessage();
				e.printStackTrace();
			}	
			return null;
		}
		
		protected void onPostExecute(Void result) {
			if(errorMsg == null)
				Toast.makeText(CustomActivity.this, getResources().getText(R.string.screenshotsaved), Toast.LENGTH_SHORT ).show();
			else
				Toast.makeText(CustomActivity.this, getResources().getText(R.string.screenshotfailed)+errorMsg, Toast.LENGTH_SHORT ).show();
		};
		
	}
	
	
}
