from imagereceiver.comm.describeddatareceiver import DescribedDataReceiver
from imagereceiver.deserialize.imagedeserializer import ImageDeserializer
from imagereceiver.misc.imagedisplayer import ImageDisplayer
from imagereceiver.misc.timetracker import TimeTracker
import traceback
import sys

def main():
    port = 8091
    ip_address = '0.0.0.0'
    receiver = DescribedDataReceiver(ip_address, port)

    displayer = ImageDisplayer()
    displayer.set_resolution((640, 480))

    timetracker = TimeTracker()

    while True:
        if not receiver.is_connected():
            receiver.get_connection()

        if receiver.is_connected():
            try:
                timetracker.stopwatch()
                img_data, description = receiver.get_image_with_description()
                passed = timetracker.stopwatch()
                # print("Loaded data - duration: "+str((passed)*1e3)+" ms")
                if img_data is not None:
                    try:
                        image = ImageDeserializer.deserialize(description, img_data)
                    except Exception as e:
                        print("Deserialization failed."+str(e))
                        traceback.print_exc(file=sys.stdout)
                        raise e


                    try:
                        displayer.display(description, image)
                    except Exception as e:
                        print("Displayer could not show image."+str(e))
                        traceback.print_exc(file=sys.stdout)

                timetracker.frame_passed()
            except KeyboardInterrupt:
                print("KeyboardInterupt. Close Receiver.")
                receiver.close()
                raise


if __name__ == '__main__':
    main()
