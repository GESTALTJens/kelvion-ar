from timeit import default_timer as timeit
import time

class TimeTracker:

    frame_start_time = time.time()
    fps_counter = 0
    fps_display_rate = 1 # seconds

    last_stopwatch_time = timeit()

    def restart_fps_tracking(self):
        self.frame_start_time = time.time()
        self.fps_counter = 0

    def frame_passed(self):
        self.fps_counter += 1
        if (time.time() - self.frame_start_time) > self.fps_display_rate:
            print("FPS: ", self.fps_counter / (time.time() - self.frame_start_time))
            self.restart_fps_tracking()

    def stopwatch(self):
        now = timeit()

        passed = (now - self.last_stopwatch_time)
        self.last_stopwatch_time = timeit()
        return passed
