import cv2
import os
import numpy as np
from imagereceiver.comm.imagetype import ImageType




class ImageDisplayer:

    MIN_DEPTH = 0
    MAX_DEPTH = 6000

   
    logo = cv2.imread('/home/jens/robolib/SanbotClient/StreamReceiver/imagereceiver/misc/logo2.png',cv2.IMREAD_UNCHANGED)
    x_offset=120
    y_offset=50

    

    resolution = None

    def __init__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        cv2.destroyAllWindows()

    def overlay(self,l_img,s_img,x_offset,y_offset):
	y1, y2 = y_offset, y_offset + s_img.shape[0]
	x1, x2 = x_offset, x_offset + s_img.shape[1]

	alpha_s = s_img[:, :, 3] / 255.0
	alpha_l = 1.0 - alpha_s

	for c in range(0, 3):
	    l_img[y1:y2, x1:x2, c] = (alpha_s * s_img[:, :, c] +
		                      alpha_l * l_img[y1:y2, x1:x2, c])
	

    def display(self, description, image):
	 

	name = 'unknown'
        frame = None




	#dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
	#gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    	#res = cv2.aruco.detectMarkers(gray,dictionary)
	#   print(res[0],res[1],len(res[2]))

    	#if len(res[0]) > 0:
	#	cv2.aruco.drawDetectedMarkers(image,res[0],res[1])
		# Print PNG
			
	
	#self.overlay(image,self.logo,self.x_offset,self.y_offset)
	##########################################################################

        if description.image_type == ImageType.DEPTH_8BIT:
            name = 'depth 8bit'
            frame = image

        elif description.image_type == ImageType.DEPTH_16BIT:
            name = 'depth 16bit'

            frame = image.astype('float32')
            max_vals = frame > self.MAX_DEPTH
            frame[max_vals] = 0
            frame = 255*(frame - self.MIN_DEPTH) / (self.MAX_DEPTH-self.MIN_DEPTH)
            frame = frame.astype('uint8')
        elif description.image_type == ImageType.RGB_8BIT:
            frame = image[:,:,::-1] # Flip the data from rgb to bgr
            name = 'rgb'

        elif description.image_type == ImageType.YUV420_888:	    
	    frame = image
            name='yuv420 888'
        else:
            print('ImageDisplayer: Unsupported image type.')

        frame = self.rescale_to_resolution(frame)
        ImageDisplayer.show_window(frame, name)

    @staticmethod
    def show_window(frame, name):
        cv2.imshow(name, frame)
        cv2.waitKey(1)

    def set_resolution(self, resolution):
        self.resolution = resolution

    def rescale_to_resolution(self, frame):
        if not self.resolution is None:
            return cv2.resize(frame, self.resolution)
        return frame
