import socket as Socket

from imagereceiver.comm.description import Description

class DescribedDataReceiver:
    ip_address = '0.0.0.0'
    connection_timeout_in_seconds = 1
    pending_connections_allowed = 1
    port = 0
    socket = None
    connection = None
    sender_address = None

    print_info = False

    def __init__(self, ip_address, port):
        self.ip_address = ip_address
        self.port = port
        print("Connecting DescribedDataReceiver with binding: "+str(self.ip_address)+", port: "+str(self.port))
        self.socket = Socket.socket(Socket.AF_INET, Socket.SOCK_STREAM)
        self.socket.setsockopt(Socket.SOL_SOCKET, Socket.SO_REUSEADDR, 1)
        self.socket.bind((self.ip_address, port))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def get_connection(self):
        try:
            print("Check for incoming connections ...")
            self.socket.settimeout(self.connection_timeout_in_seconds)
            self.socket.listen(self.pending_connections_allowed)
            self.connection, self.sender_address = self.socket.accept()
            print("Connected to: " + str(self.sender_address))
        except Socket.timeout:
            self.close_connection()
        except:
            raise

    def is_connected(self):
        return self.connection is not None

    def get_image_with_description(self):
        if self.print_info:
            print("Connected to " + str(self.sender_address))
            print("Reading description ...")

        description_as_byte_array = self.connection.recv(Description.get_size_as_byte_array())

        if len(description_as_byte_array) <= 0:
            print("Received data are empty, connection lost.")
            self.close_connection()
            return None, None

        description = Description(description_as_byte_array)

        if self.print_info:
            print("img type: "+str(description.image_type))
            print("img height: "+str(description.height))
            print("img width: "+str(description.width))
            print("img size:" +str(description.data_size))

	    print("Marker recognized: "+str(description.marker))
	    print("Image Number: "+str(description.image_nr))

            print("Reading data ...")

        data = bytearray()
        bytes_to_receive = description.data_size
        while bytes_to_receive > 0:
            part = self.connection.recv(bytes_to_receive)
            data += part
            bytes_to_receive -= len(part)

        return data, description

    def close(self):
        self.close_connection()
        self.close_socket()

    def close_connection(self):
        if self.connection is not None:
            self.connection.close()
        self.connection = None
        self.sender_address = None

    def close_socket(self):
        if self.socket is not None:
            self.socket.close()
