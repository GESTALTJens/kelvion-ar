import codecs

class Description:
    image_type = 0
    width = 0
    height = 0
    compression = 0
    timestamp = 0
    data_size = 0
    encoding = 'big'
    marker = 0
    image_nr = 0

    def __init__(self, byte_array=None):
        print("len descriptor byte array: "+str(len(byte_array)))
        if byte_array is not None:
            self.set_values_from_bytes(byte_array)

    def set_values_from_bytes(self, byte_array):
        self.compression = int(codecs.encode(byte_array[:1], 'hex'), 16) #int.from_bytes(byte_array[:1], byteorder=self.encoding)
        self.image_type = int(codecs.encode(byte_array[1:5], 'hex'), 16) #int.from_bytes(byte_array[1:5], byteorder=self.encoding)
        self.width = int(codecs.encode(byte_array[5:9], 'hex'), 16) #int.from_bytes(byte_array[5:9], byteorder=self.encoding)
        self.height = int(codecs.encode(byte_array[9:13], 'hex'), 16)#int.from_bytes(byte_array[9:13], byteorder=self.encoding)
        self.timestamp = int(codecs.encode(byte_array[13:21], 'hex'), 16)  #int.from_bytes(byte_array[13:21], byteorder=self.encoding)
        self.data_size = int(codecs.encode(byte_array[21:25], 'hex'), 16) #int.from_bytes(byte_array[21:], byteorder=self.encoding)

        #0,1,2: 0: nix anzeigen, 1: erster datensatz, 2: zweiter datensatz
        self.marker = int(codecs.encode(byte_array[25:29], 'hex'), 16) #int.from_bytes(byte_array[21:], byteorder=self.encoding)
        #img nr aus dem jew. datensatz
        self.image_nr = int(codecs.encode(byte_array[29:], 'hex'), 16) #int.from_bytes(byte_array[21:], byteorder=self.encoding)
        
        print("width: "+str(self.width))
        print("height: "+str(self.height))
        print("data_size: "+str(self.data_size))
        print("timestamp: "+str(self.timestamp))
        print("image_type: "+str(self.image_type))
        print("compression: "+str(self.compression))
        print("marker: "+str(self.marker))

	print("Marker recognized: "+str(self.marker))
	print("Image Number: "+str(self.image_nr))

    def as_byte_array(self):
        array = bytearray().join([
            self.compression.to_bytes(1, self.encoding),
            self.image_type.to_bytes(4, self.encoding),
            self.width.to_bytes(4, self.encoding),
            self.height.to_bytes(4, self.encoding),
            self.timestamp.to_bytes(8, self.encoding),
            self.data_size.to_bytes(4, self.encoding),
	    self.marker.to_bytes(4, self.encoding),
	    self.image_nr.to_bytes(4, self.encoding)
            ])

        return array

    @staticmethod
    def get_size_as_byte_array():
        # Depends on the size from the robot

        # 1 for compression
        # 4 for image_type
        # 4 for width
        # 4 for height
        # 8 byte for timestamp
        # 4 for data size
	#...
        return 8 + 4 + 4 + 4 + 4 + 4 + 4  + 1
