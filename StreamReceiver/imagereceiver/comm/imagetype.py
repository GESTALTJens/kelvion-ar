from enum import IntEnum


# ImageTypes are preset in Sanbotlib
class ImageType(IntEnum):
    DEPTH_16BIT = 0
    DEPTH_8BIT = 1
    RGB_8BIT = 2
    YUV420_888 = 3
    JPEG = 4
