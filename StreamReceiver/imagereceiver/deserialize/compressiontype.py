from enum import IntEnum


# CompressionType are preset in Sanbotlib
class CompressionType(IntEnum):
    RAW = 0
    PNG = 1
    ZIP = 2
    BLOSC = 3
    JPEG = 4

