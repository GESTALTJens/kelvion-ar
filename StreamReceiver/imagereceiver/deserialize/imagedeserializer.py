import zlib
import cv2
import numpy as np
from PIL import Image as im
import io

from imagereceiver.comm.imagetype import ImageType
from imagereceiver.deserialize.compressiontype import CompressionType

np.set_printoptions(threshold=np.inf)

class ImageDeserializer:

    @staticmethod
    def deserialize(description, data):
        decompressed_data = None

        if description.compression == CompressionType.RAW:
            decompressed_data = data
        elif description.compression == CompressionType.PNG:
            decompressed_data = ImageDeserializer.decompress_png(description, data)
        elif description.compression == CompressionType.JPEG:
            print("decompress JPEG")
            decompressed_data = ImageDeserializer.decompress_jpeg(description, data)
            print("decompressed jpeg data shape: "+str(decompressed_data.shape))
        elif description.compression == CompressionType.ZIP:
            decompressed_data = ImageDeserializer.decompress_zip(data)

        return ImageDeserializer.extract_image(description, decompressed_data)

    @staticmethod
    def extract_image(description, data):
        img_size = (description.height, description.width)

        if description.image_type == ImageType.DEPTH_16BIT:
            frame = np.frombuffer(data, np.uint16)

        if description.image_type == ImageType.DEPTH_8BIT:
            frame = np.frombuffer(data, np.uint8)

        if description.image_type == ImageType.RGB_8BIT:
            img_size = (description.height, description.width,  3)
            frame = np.frombuffer(data, np.uint8)
            #img_size = (description.height, description.width,  3)
            #frame = ImageDeserializer.decompress_ycbcr(description, data) #(np.frombuffer(data, np.uint8)
            #return frame



        if description.image_type == ImageType.YUV420_888:
            img_size = (description.height, description.width,  3)
            frame = ImageDeserializer.decompress_ycbcr(description, data) #(np.frombuffer(data, np.uint8)
            return frame


        return  np.reshape(frame, img_size)

    @staticmethod
    def decompress_png(description, data):
        raw = np.array(data)

        if ImageDeserializer.is_depth_image(description):
            return cv2.imdecode(raw, cv2.IMREAD_ANYDEPTH)
        if ImageDeserializer.is_color_image(description):
            return cv2.imdecode(raw, cv2.IMREAD_COLOR)

        return raw

    @staticmethod
    def decompress_jpeg(description, data):

        raw = np.array(data)

        if ImageDeserializer.is_depth_image(description):
            res= cv2.imdecode(raw, cv2.IMREAD_ANYDEPTH)
            return res
        if ImageDeserializer.is_color_image(description):
            print("imdecode color")
            res= cv2.imdecode(raw, cv2.IMREAD_COLOR)
            print("res: "+str(res))
            return res

        return raw


    @staticmethod
    def decompress_ycbcr(description, data):
	Y = 0
	Cb = 1
	Cr = 2
	raw = np.array(data)
	YCbCr=list(data) # flat list of tuples
        YCbCr = im.frombytes('RGB', (int(640),int(480)),raw )
	# reshape
	imYCbCr=np.array(YCbCr)
	#imYCbCr = np.reshape(YCbCr, (int(480/2),int( 640/2), 3))
	# Convert 32-bit elements to 8-bit
	imYCbCr = imYCbCr.astype(np.uint8)

	########################
	#buf =  np.array(data)
	#rewidth = description.width
        #reheight = description.height*1.5
	#shape=( reheight,rewidth)
        #bla = np.reshape(buf, shape)
	#############################

	#bgr = cv2.cvtColor(imYCbCr, cv2.COLOR_YCR_CB2BGR)
	#xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
	#rgb = imYCbCr.astype(np.float)
	#rgb[:,:,[1,2]] -= 128
	#rgb = rgb.dot(xform.T)
	#np.putmask(rgb, rgb > 255, 255)
	#np.putmask(rgb, rgb < 0, 0)
	return imYCbCr#np.uint8(rgb)

    @staticmethod
    def decompress_yuv(description, data):
        """
        converts YUV_420_888 into RGB8
        """
        buf =  np.array(data) #np.fromstring(message,dtype=np.uint8)

	#cv2.waitKey(1000)
        rewidth = description.width
        reheight = description.height+description.height/2
        #rewidth = width/2
        #reheight = reheight/2
        shape=( rewidth,reheight)
        print("new shape: "+str(shape))
        #bla = np.reshape(buf, shape)

        #see all possible https://docs.opencv.org/3.1.0/d7/d1b/group__imgproc__misc.html
        bgr = cv2.cvtColor(buf, 43)
        #COLOR_YUV2RGB_UYVY
        #bgr = cv2.cvtColor(bla, cv2.COLOR_YUV2RGB_UYVY)

        return bgr

    @staticmethod
    def is_color_image(description):
        return description.image_type == ImageType.RGB_8BIT

    @staticmethod
    def is_depth_image(description):
        return description.image_type == ImageType.DEPTH_16BIT or \
            description.image_type == ImageType.DEPTH_8BIT

    @staticmethod
    def decompress_zip(data):
        return zlib.decompress(data)
