import os
import sys
import traceback
import time

import cv2
import numpy as np
from flask import Flask, render_template, Response

from imagereceiver.comm.describeddatareceiver import DescribedDataReceiver
from imagereceiver.deserialize.imagedeserializer import ImageDeserializer
from imagereceiver.misc.imagedisplayer import ImageDisplayer
from imagereceiver.misc.timetracker import TimeTracker
from multiprocessing import Process, Queue

app = Flask(__name__, template_folder='templates')
img_queue=Queue(maxsize=1)
descriptors_queue=Queue(maxsize=1)
overlays = []


def get_data_loop(img_queue, descriptors_queue):
    print("Init Image receiver")
    port = 8091
    ip_address = '0.0.0.0'
    receiver = DescribedDataReceiver(ip_address, port)

    # displayer = ImageDisplayer()
    # displayer.set_resolution((640, 480))

    timetracker = TimeTracker()

    while True:
        if not receiver.is_connected():
            receiver.get_connection()

        if receiver.is_connected():
            try:
                timetracker.stopwatch()

                # only put new img if all queues are empty
                if not img_queue.full() and not descriptors_queue.full():

                    img_data, description = receiver.get_image_with_description()
                    passed = timetracker.stopwatch()
                    # print("Loaded data - duration: "+str((passed)*1e3)+" ms")

                    if img_data is not None:
                        try:
                            image = ImageDeserializer.deserialize(description, img_data)
                            img_queue.put_nowait((image, description))

                        except Exception as e:
                            print("Deserialization failed."+str(e))
                            traceback.print_exc(file=sys.stdout)
                            raise e

                    timetracker.frame_passed()

            except KeyboardInterrupt:
                print("KeyboardInterupt. Close Receiver.")
                receiver.close()
                raise

def gen_video():
    global curr_frame
    global img_queue
    global overlays

    fr_counter=0

    while True:

        try:
            frame, description = img_queue.get()
            dataset_num = description.marker

            if dataset_num > 0:

                img_num = description.image_nr
                overlay = overlays[dataset_num-1][img_num]

                # merge overlay
                merged_frame = frame

                alpha = overlay[...,3]
                opaque = alpha > 0
                merged_frame[opaque, 0] = overlay[opaque, 2]
                merged_frame[opaque, 1] = overlay[opaque, 1]
                merged_frame[opaque, 2] = overlay[opaque, 0]

                frame = merged_frame

            ok, encoded_img=cv2.imencode("*.png", frame[...,::-1])
            frame=encoded_img.tobytes()

            yield (b'--frame\r\n'
                b'Content-Type: image/png\r\n\r\n' + frame + b'\r\n')
        except:
            print("error on gen_video. ")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/video_feed')
def video_feed():
    return Response(gen_video(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/video_overlay')
def video_overlay():
    return Response(gen_overlay(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')



if (__name__=="__main__"):
    # load overlays
    dir_path = os.path.dirname(os.path.realpath(__file__))
    img_nos = [range(1,9), range(1,20)]
    w = 640
    h = 480

    for dataset in range(len(img_nos)):
        current_ds = []

        for img_no in range(len(img_nos[dataset])):

            dataset_num = dataset+1
            img_num = img_no+1
            img_filename = dir_path + "/static/imgs_daten"+str(dataset_num)+"/r_"+str(img_num).zfill(4)+".png"
            print("img overlay filename: "+str(img_filename))

            overlay = cv2.resize(cv2.imread(img_filename, cv2.IMREAD_UNCHANGED),
                                (w,h))
            current_ds.append(overlay)

        overlays.append(current_ds)

    get_data_process = Process(target=get_data_loop, args=(img_queue, descriptors_queue))
    get_data_process.daemon = True
    get_data_process.start()

    app.run(host='0.0.0.0', port=5011, threaded=True)
