## Installation
Docker image bauen
```
docker build -f dockerfile --tag kelvion . 
```

Docker image starten
```
docker run -it -p 5011:5011 -p 8091:8091 -v `pwd`:/app kelvion python /app/restream_images_for_web.py
```

Browser öffnen und `localhost:5011` eingeben
